import pytest
pytest.main(['test_main.py', '-x', '-v', '-p', 'no:warnings'])
"""
    Ao completar uma função, querendo executar, vá ao arquivo 
    teste_main.py e retire o @pytest.mark.skip do teste correspondente.
"""

from typing import List
from collections import Counter

def maior(array: List[int]):
    """
    Escreva uma função que recebe um array de inteiros e 
    retorna o índice do maior valor.
    - Não usar funções que fazem o que foi pedido, como max, etc.
    """
    max_value = 0
    max_index = 0
    for index, value in enumerate(array):
        if max_value < value:
            max_value = value
            max_index = index

    return max_index

def reverte(texto: str):
    """
    Escreva uma função que recebe uma string e 
    retorna o reverso dela. Exemplo: reverter(“abc”) -> “cba”.
    - Não usar funções que fazem o que foi pedido, como reversed,     [::-1], etc.
    """
    # code here
    new_text: str = ''
    
    for index in range(len(texto) -1, -1, -1):
        new_text += texto[index]
    return new_text

def duplicados(texto: str):
    """
    Escreva uma função que retorna a contagem dos caracteres repetidos num texto.
    - O texto pode conter caracteres ou números. Não haverá caracter acentuado.
    - A contagem deve ser case-insensitive (ex.: 'a' é o mesmo que 'A')
    
    Exemplos
    "abcde" -> 0 # nenhum caracter repete
    "aabbcde" -> 2 # 'a' e 'b'
    "aabBcde" -> 2 # 'a' repete e 'b' repete (`b` e `B`)
    "inconstitucional" -> 5 # 'i', 'n', 'c', 'o' e 't' repetem
    "insistir" -> 2 # 'i' 3x e 's' 2x
    "aaaaaaaaaab" -> 1 # 'a' repete 10 vezes

    - Pode usar o que quiser, que seja do python. 
    - Se precisar adicione o import no topo do arquivo.
    """
    
    counter = Counter(texto.lower())
    repeted_letters = 0
    for i in counter.values():
        if i > 1:
            repeted_letters += 1
    return repeted_letters