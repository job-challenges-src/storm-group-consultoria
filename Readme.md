# Desafio técnico Strom Group

#### Cargo: Python backend senior

## Live code

### 1° Desafio
```python
"""
    Escreva uma função que recebe um array de inteiros e retorna o índice do maior valor.
    - Não usar funções que fazem o que foi pedido, como max, etc.
"""
```
### 2° Desafio
```python
"""
    Escreva uma função que recebe uma string e retorna o reverso dela. Exemplo: reverter(“abc”) -> “cba”.
    - Não usar funções que fazem o que foi pedido, como reversed,     [::-1], etc.
"""
```
### 3° Desafio
```python
"""
    Escreva uma função que retorna a contagem dos caracteres repetidos num texto.
    - O texto pode conter caracteres ou números. Não haverá caracter acentuado.
    - A contagem deve ser case-insensitive (ex.: 'a' é o mesmo que 'A')
    
    Exemplos
    "abcde" -> 0 # nenhum caracter repete
    "aabbcde" -> 2 # 'a' e 'b'
    "aabBcde" -> 2 # 'a' repete e 'b' repete (`b` e `B`)
    "inconstitucional" -> 5 # 'i', 'n', 'c', 'o' e 't' repetem
    "insistir" -> 2 # 'i' 3x e 's' 2x
    "aaaaaaaaaab" -> 1 # 'a' repete 10 vezes

    - Pode usar o que quiser, que seja do python. 
    - Se precisar adicione o import no topo do arquivo.
"""
```

# Para rodar o projeto

- Instale as dependências

```bash
pip install -r requirements.txt
```

- Rode os tests

```bash
pytest -vv
```

## Aprovado!



