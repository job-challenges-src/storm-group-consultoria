from src.main import maior, reverte, duplicados
import pytest


def test_maior():
    array = [3, 6, 4, 2, 1, 7, 6, 2, 3, 5]
    result = maior(array)
    assert result == 5


@pytest.mark.parametrize('texto', [
    ('jT9cuH'),
    ('BIXHdEp6d'),
    ('vfWwDti1uQrsAR5'),
    ('e5Xv3DVn'),
    ('7LlmLa5eo8FH'),
    ('Kr3'),
    ('L3VsGBEBusgRMws'),
    ('jUqSwPhh0uhlv'),
    ('Sh'),
    ('CEKIaR')])
def test_reverte(texto):
    result = reverte(texto)
    assert result == texto[::-1]


@pytest.mark.parametrize('texto, expected', [
    ('aaaab', 1),
    ('p1CGnoE1px6RTJv', 2),
    ('mwdMACExCcGj3HMBydoh2a1NqU7XG7bWLJnPW', 12),
    ('69az5lHOTRltCDVqA0P9VBf0xQC1ID', 9),
    ('bROpBqiPHT4asEl3qUUBRWAFHaUsKsTr7ZEEJO1jZofy4O', 15),
    ('BbQShWejxcE600Jb6yerfkxGMeevJhW9', 8),
    ('eStwfy5bbHpRYdEMlW5O', 5),
    ('mSMQ9F4lc9X', 2),
    ('7hmw9FvbjfwSRB9FQ', 4),
    ('uvbsTSeLZfyAQQ0tTw53mW5', 5),
    ('ltEvwVGiqEVDfYux6SbV6Wr98EK599Ov4mzLok49kuhl1Pq4TZ', 13),
    ('kFLtUitQwM88iTV5yjESnYfEje3jEfxtko', 8),
    ('RNao7UvI9uyYnXwn', 3),
    ('DPvp3uKvICN41G231z2J1VoAa1gwmy4HSvmjc08M1FxRJUNz', 14),
    ('KMzPBqpkd5p43WzcIU5PNfUOJ', 5),
    ('VXjBbBNjma51EOPu3aWXx05', 5),
    ('g9C99oQtWcPpzOBajlbqKa30vR3vAkN', 10),
    ('5Srao23DKz7eS1iFWav1', 3),
    ('icSyvc04e6eIHErWLSrkUz4JrG56lO9eNkPBkKmPDc8HhjNX', 13),
    ('LXekn346LWIlko1rw4oy57jB', 5),
    ('G48dQKFGgyfUYycL40ixcWQ7U', 7),
    ('JmiFvQhHTsUl713sB79CUdKrB7Sf73F', 7),
    ('DIcaFKi0DMoyBddI', 2),
    ('hMpPfDcb822etbjwddYQMaTxM', 6),
    ('ftzh6TILWCYgoe8wCC629lLcOMEi', 8),
    ('mlgO03', 0),
    ('EcXQmscZy4odaQ1qZv5EV', 5),
    ('pjmO5qDGr', 0),
    ('VZQ5IDyPtqv2p8vhyFEDdllhPB7TgiST90YHh0yFMDcEZWcM', 15),
    ('0EYcF0UD0Ria9BJgCnKzxx3xPjo9HKE1LdL8VhJ7peX', 11),
    ('LyCpCCB4J6Pu4gsEYQChNSMA', 5),
    ('KqmDMMO6PK1nwL8AopRQ', 5),
    ('81ukZAA5cGg2K3KI3NKXiiRuLsMn20xn', 9),
    ('yI4Wi8HvWuDJWY7stb8uIbqxpmp5lKYwOQO10qTR4GPHrzM16', 15),
    ('eMSOJyP510tPryTjaMArac5CWFNIe9425c', 10),
    ('ror97mB2au4W6', 1),
    ('47pT6IFiBh9yXLpfGjHAPR3H', 4),
    ('tiNy2n', 1),
    ('KefxzCLYUJshhjWwyUzJHoTJq', 6),
    ('a9OTbjG9SMqfBxrr5J8Uydk8opp0k6lsXCNO', 10),
    ('PIwpsUF0s', 2),
    ('fqRUj7YZ73', 1),
    ('cnGf5FqO5450iL7f6A2lX5FTFe0SVIp', 5),
    ('naB0EPMPs8wqpFyP7pa07uOTpQojkSKmorSs5bG', 10),
    ('CdG5c87JeGQlTAk1KbotgdM8vWDWQnXz1duVj7VDuJh7Im', 14),
    ('WbxZOVe5huaFhlyVHg0I1uTwfBpCw', 6),
    ('gw5YEhPa9P1LHaw4MEpzV', 5),
    ('nAahzwle', 1),
    ('dEuPHMdoj8vmlUOMI0LfEfuXIHrlortFUvHP5GVXzwf4hyZd', 14),
    ('VG6z7hmmvd', 2),
    ('KAX6bj9Bp85WV93h9JK7t9FDZfTSU6W791tkFm', 9),
])
def test_duplicados(texto, expected):
    assert duplicados(texto) == expected